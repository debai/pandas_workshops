# Hello Pandas!

This is a git repo of pandas workshops which were held at McMaster University for an audience of undergrad students. There is no copyright protection on any of the material, so use it as you wish. If you have any questions, feel free to shoot an email to David Bai: "baid 'at' mcmaster 'dot' ca"

# Instructions

#### Using Your Own Computer (recommended):

0. Download this git repo to your desktop (either the zip file or git clone) and unzip it to your desktop

1. Download and install python3 if you don’t have it already:
    - [Python 3.8 64 Bit (Mac)](https://www.python.org/ftp/python/3.8.1/python-3.8.1-macosx10.9.pkg)
    - [Python 3.8 64 Bit (Windows)](https://www.python.org/ftp/python/3.8.1/python-3.8.1-webinstall.exe)

2. Open Terminal (Mac/Linux) or Open Powershell (Windows)

3. Installing packages for Python:
    - Type: pip3 install numpy
    - Type: pip3 install pandas
    - Type: pip3 install jupyter
    - If the above doesn’t work type "pip install *package*")

4. Type: jupyter notebook (a browser window should pop up)

5. In the browser window, navigate to desktop / pandas_workshops / workshop1

6. Open the code_unfinished.ipynb file

7. Minimize the terminal / Powershell window (DO NOT CLOSE)


#### Using a Lab Computer:

0. Download this git repo to your desktop (either the zip file or git clone) and unzip it to your desktop

1. Open search Type Jupyter Notebook then open it

2. Wait a long time for the black screen to do something

3. The black screen should eventually say: “….. or copy and paste one of these URLS:”

4. Copy and paste one of the urls in a browser window

5. Navigate to desktop / pandas_workshops / workshop1

6. Open the code_unfinished.ipynb file

7. Minimize the black window (DO NOT CLOSE)
